<?php

namespace App\Http\Controllers\Api;

use App\CreditMemo;
use App\Mail\MailCreditMemo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Validator;

class CreditMemoController extends Controller
{
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            // Check is id exist
            'client_id' => 'required|exists:users,id',
            'company_info'=>'required',
            'cart_items'=>'required',
            'subtotal'=>'required',
            'discount'=>'required',
            'tax'=>'required',
            'grand_total'=>'required',
            'invoice_no'=>'required',
            'credit_memo_no'=>'required',
            'issue_date'=>'required',
            'due_date'=>'required',
            'client_info'=>'required',
            'status'=>'required',
        ]);

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return response()->json(['status'=>'failed', 'message'=>$error]);
        }

        $allData=$request->all();

        if ($request->hasFile('bg_image')){
            $path=$request->file('bg_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(400, 250)->encode();
            Storage::put($path, $image);
            $allData['bg_image']=$path;
        }

        $allData['company_info']=json_decode($request->company_info,true);
        $allData['client_info']=json_decode($request->client_info,true);
        $allData['cart_items']=json_decode($request->cart_items,true);

        $credit_memo=CreditMemo::create($allData);
        $credit_memo->credit_memo_id=Str::random(50).$credit_memo->id;
        $credit_memo->save();

        return response()->json(['status'=>'success',]);
    }

    public function sent_credit_memo(Request $request){
        $validator = Validator::make($request->all(), [
            // Check is id exist
            'client_id' => 'required|exists:users,id',
            'company_info'=>'required',
            'cart_items'=>'required',
            'subtotal'=>'required',
            'discount'=>'required',
            'tax'=>'required',
            'grand_total'=>'required',
            'invoice_no'=>'required',
            'credit_memo_no'=>'required',
            'issue_date'=>'required',
            'due_date'=>'required',
            'client_info'=>'required',
            'status'=>'required',
            'email_address'=>'required',
            'message'=>'required',
            'subject'=>'required',
        ]);

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return response()->json(['status'=>'failed', 'message'=>$error]);
        }

        $allData=$request->all();

        if ($request->hasFile('bg_image')){
            $path=$request->file('bg_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(400, 250)->encode();
            Storage::put($path, $image);
            $allData['bg_image']=$path;
        }

        $allData['company_info']=json_decode($request->company_info,true);
        $allData['client_info']=json_decode($request->client_info,true);
        $allData['cart_items']=json_decode($request->cart_items,true);

        $credit_memo=CreditMemo::create($allData);
        $credit_memo->credit_memo_id=Str::random(50).$credit_memo->id;
        $credit_memo->save();

        $allData['title']=$request->subject;
        $allData['message']=$request->message;
        $allData['url']=env('APP_URL').'/credit-memo/'.$credit_memo->credit_memo_id;
        Mail::to($request->email_address)->send(new MailCreditMemo($allData));

        return response()->json(['status'=>'success',]);
    }
}
