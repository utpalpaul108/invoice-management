<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProductCollection;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(){
        return new ProductCollection(Product::all());
    }
}
