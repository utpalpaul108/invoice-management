<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ClientCollection;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
    //    For Login
    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'email'=>'required|email',
            'password'=>'required'
        ]);

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return response()->json(['status'=>'failed', 'message'=>$error]);
        }

        if (Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'status'=>1])){
            $user=Auth::user();
            $tokenResult = $user->createToken('Personal Access Token')->accessToken;
            return response()->json(['access_token'=>$tokenResult]);
        }
        else{
            return response()->json(['status' => 'Unauthorized'], 401);
        }
    }

    //    For user response
    public function profile(Request $request){
        $user=$request->user();
        return response()->json(['name'=>$user->name]);
    }

    //     For logout
    public function logout(Request $request){
        $request->user()->token()->revoke();
        return response()->json(['status'=>'success']);
    }

    public function company_info(){
        $company_info['company_name']=setting('company_name');
        $company_info['phone_no']=setting('phone_no');
        $company_info['street']=setting('street');
        $company_info['city']=setting('city');
        $company_info['zipcode']=setting('zipcode');
        $company_info['country']=setting('country');

        return response()->json($company_info);
    }

    public function clients(){
        return new ClientCollection(User::where([['user_type','client'],['status',1]])->get());
    }

    public function sendInvoice(){

    }

}
