<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Spatie\Permission\Models\Role;
use Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function login(){
        return view('admin.user.login');
    }

    public function logout(){
        Auth::logout();
        return redirect()->action('Admin\UserController@login');
    }

    public function authenticate(Request $request){
        if (Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'user_type'=>'admin','status'=>1])){
            return redirect()->action('Admin\DashboardController@index');
        }
        else{
            flash('Invalid username or password')->error();
            return redirect()->back();
        }
    }
}
