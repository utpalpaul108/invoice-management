<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Validation\Rule;


class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients=User::where([['user_type','client'],['status',1]])->get();
        return view('admin.clients.index',compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'phone_no' => 'required',
            'company_name' => 'required',
            'business_contact_first_name' => 'required',
            'business_contact_last_name' => 'required',
            'business_position' => 'required',
            'status' => 'required|min:0|max:1',
            'street'=> 'required',
            'city'=>'required',
            'zipcode'=>'required',
            'country'=>'required'
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $allData=$request->all();
        $allData['user_type']='client';
        $allData['password']=bcrypt($request->password);

        if ($request->hasFile('profile_image')){
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $allData['profile_image']=$path;
        }

        User::create($allData);
        flash('New client created successfully');
        return redirect()->action('Admin\ClientController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client=User::findOrFail($id);
        return view('admin.clients.edit',compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation=Validator::make($request->all(), [
            'name' => 'required',
            'email' => [
                'required',
                Rule::unique('users')->ignore($id),
            ],
            'phone_no' => 'required',
            'company_name' => 'required',
            'business_contact_first_name' => 'required',
            'business_contact_last_name' => 'required',
            'business_position' => 'required',
            'street'=> 'required',
            'city'=>'required',
            'zipcode'=>'required',
            'country'=>'required',
            'status' => 'required|min:0|max:1',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $client=User::findOrFail($id);
        $client->name=$request->name;
        $client->email=$request->email;
        $client->phone_no=$request->phone_no;
        $client->company_name=$request->company_name;
        $client->business_contact_first_name=$request->business_contact_first_name;
        $client->business_contact_last_name=$request->business_contact_last_name;
        $client->business_position=$request->business_position;
        $client->street=$request->street;
        $client->city=$request->city;
        $client->zipcode=$request->zipcode;
        $client->country=$request->country;
        $client->status=$request->status;

        if ($request->hasFile('profile_image')){
            Storage::delete($client->profile_image);
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $client->profile_image=$path;
        }

        if (isset($request->password)){
            $client->status=bcrypt($request->password);
        }
        $client->save();
        flash('Client updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        flash('Client deleted successfully');
        return redirect()->action('Admin\ClientController@index');
    }
}
