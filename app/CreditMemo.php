<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditMemo extends Model
{
    use SoftDeletes;

    protected $fillable=['credit_memo_id','client_id','company_info','cart_items','subtotal','discount','tax','grand_total','notes','terms','bg_image','invoice_no','credit_memo_no','issue_date','due_date','reference','client_info','status'];

    protected $casts=['company_info'=>'array', 'client_info'=>'array', 'cart_items'=>'array'];

    public function client(){
        return $this->belongsTo('App\User','client_id');
    }
}
