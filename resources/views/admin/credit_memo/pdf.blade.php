<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Credit Memo</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
{{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">--}}
{{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">--}}
    <style>

        /* start */
        body {
            overflow-x: hidden;
            font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
            color: #fff;
            font-size: 14px;
            margin: 0;
            color: #000;
            background-color: #fff; }

        .primary-color {
            color: green; }

        .primary-bg {
            background-color: green; }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            margin-top: 0; }

        h1 {
            font-size: 36px;
            margin-bottom: 0px; }

        h2 {
            font-size: 24px;
            font-weight: bold;
            margin-bottom: 8px; }

        h3 {
            font-size: 18px;
            font-weight: 600;
            margin-bottom: 15px; }

        h6 {
            font-size: 16px;
            font-weight: bold; }

        p {
            font-size: 15px;
            margin-bottom: 0; }

        section {
            padding-top: 45px;
            padding-bottom: 45px; }

        a {
            -webkit-transition: 0.3s;
            -moz-transition: 0.3s;
            -ms-transition: 0.3s;
            -o-transition: 0.3s;
            transition: 0.3s; }

        img {
            max-width: 100%;
            height: auto; }

        *,
        *::after,
        *::before {
            box-sizing: border-box; }

        .form-control,
        input,
        label {
            overflow-wrap: break-word;
            word-wrap: break-word;
            word-break: break-word;
            -ms-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto; }

        a, abbr, acronym, address, applet, article, aside, audio, b, big, blockquote, body, button, canvas, caption, center, cite, code, dd, del, details, dfn, div, dl, dt, em, embed, fieldset, figcaption, figure, footer, form, h1, h2, h3, h4, h5, h6, header, hgroup, html, i, iframe, img, input, ins, kbd, label, legend, li, mark, menu, nav, object, ol, output, p, pre, q, ruby, s, samp, section, small, span, strike, strong, sub, summary, sup, table, tbody, td, textarea, tfoot, th, thead, time, tr, tt, u, ul, var, video {
            margin: 0;
            padding: 0;
            border: 0;
            vertical-align: baseline;
            -webkit-text-size-adjust: 100%;
            -moz-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%; }

        .hidden {
            display: none; }

        label {
            margin-bottom: 0; }

        /*inline input */
        .ic-inline .form-group,
        .ic-inline .ic-semicolon {
            display: inline-block; }

        /* input direction */
        .input-right {
            direction: rtl; }
        .input-right .chosen-results {
            direction: ltr;
            text-align: left; }

        .table th,
        .table td {
            white-space: nowrap;
            text-overflow: ellipsis;
            padding: 0 !important; }

        table {
            width: 490px !important;
            table-layout: fixed; }
        table th:nth-child(2) {
            text-align: right; }
        table th:nth-child(3) {
            text-align: right; }
        table th:nth-child(4) {
            text-align: right; }
        table td:nth-child(2) {
            text-align: right; }
        table td:nth-child(3) {
            text-align: right; }
        table td:nth-child(4) {
            text-align: right; }
        table td input {
            text-align: right; }

        .form-group {
            margin-bottom: 0; }


        /* responsive start */

        a {
            display: inline-block;
            text-decoration: none !important; }

        .ic-btn {
            border: none;
            border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            background-color: transparent;
            font-size: 24px; }

        .ic-btn-primary {
            background-color: green;
            cursor: pointer;
            color: #fff;
            height: 50px;
            line-height: 50px;
            text-align: center;
            padding-left: 25px;
            padding-right: 25px; }

        .ic-top-heading {
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-justify-content: space-between;
            -moz-justify-content: space-between;
            -ms-justify-content: space-between;
            justify-content: space-between;
            -ms-flex-pack: space-between;
            -webkit-flex-wrap: wrap;
            -moz-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-align-items: center;
            -moz-align-items: center;
            -ms-align-items: center;
            align-items: center;
            margin-top: 25px;
            margin-bottom: 25px; }
        .ic-top-heading h1 {
            margin-bottom: 0; }
        .ic-top-heading button {
            margin-left: 10px; }
        .ic-top-heading .ic-add-discount-full {
            display: inline-block;
            color: #062942; }

        .ic-invoice-wrapper {
            max-width: 835px;
            margin-left: auto;
            margin-right: auto; }

        .ic-invoice {
            position: relative;
            margin-bottom: 50px;
            border: 1px solid #cdd4d9;
            box-shadow: 2px 2px 0 rgba(6, 41, 66, 0.1);
            color: #000;
            page-break-after: always;
            border-radius: 10px;
            background: transparent; }
        .ic-invoice .editable-table [type="text"] {
            background: none;
            border: none;
            display: block;
            width: 100%; }
        .ic-invoice .output {
            white-space: normal; }
        .ic-invoice .heading {
            padding: 20px 50px;
            background-color: #34495e;
            border-radius: 10px 10px 0 0;
            color: #fff; }
        .ic-invoice .heading #h-cn {
            font-size: 24px; }
        .ic-invoice .heading .ic-form-inner {
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-justify-content: space-between;
            -moz-justify-content: space-between;
            -ms-justify-content: space-between;
            justify-content: space-between;
            -ms-flex-pack: space-between; }
        .ic-invoice .heading .ic-right {
            direction: rtl;
            text-align: right; }
        .ic-invoice .heading .ic-right .ic-state,
        .ic-invoice .heading .ic-right .ic-semicolon,
        .ic-invoice .heading .ic-right .ic-city {
            display: inline-block; }
        .ic-invoice .heading .ic-right .ic-semicolon {
            color: #C6B3D9; }
        .ic-invoice .heading .ic-right .form-control {
            color: #fff;
            direction: ltr;
            text-align: right; }
        .ic-invoice .ic-invoice-banner {
            height: 200px;
            position: relative;
            font-size: 18px;
            overflow: hidden; }
        .ic-invoice .ic-invoice-banner .inner img#banner-img {
            height: auto;
            width: 100%; }
        .ic-invoice .ic-invoice-banner .inner .button {
            position: absolute;
            bottom: 0;
            padding: 5px;
            width: 100%;
            text-align: center;
            background: rgba(243, 249, 253, 0.8);
            cursor: auto;
            display: none; }
        .ic-invoice .ic-invoice-body {
            position: relative;
            padding: 50px; }
        .ic-invoice .ic-invoice-body .ic-semicolon {
            color: #CDCDCD; }
        .ic-invoice .ic-invoice-body table tr:first-child th {
            padding: 0 0 10px 10px !important;
            border-top: 0 !important; }
        .ic-invoice .ic-invoice-body table tr:first-child th:first-child {
            padding-left: 0px !important; }
        .ic-invoice .ic-invoice-body table .del-cnt {
            padding: 10px !important;
            padding-left: 10px !important;
            border-top: 0 !important; }
        .ic-invoice .ic-invoice-body table .del-cnt .del {
            background: transparent;
            cursor: pointer; }
        .ic-invoice .ic-invoice-body table .del-cnt .del i {
            color: #828282;
            opacity: .5;
            font-size: 15px;
            -webkit-transition: 0.1s ease-in-out;
            -moz-transition: 0.1s ease-in-out;
            -ms-transition: 0.1s ease-in-out;
            -o-transition: 0.1s ease-in-out;
            transition: 0.1s ease-in-out; }
        .ic-invoice .ic-invoice-body table th.ic-text ic-invoice-label.last {
            border-bottom: 0 !important; }
        .ic-invoice .ic-invoice-body .box-shadow {
            box-shadow: 0 0 0 2px #cdd4d9 !important; }
        .ic-invoice .ic-invoice-body table td:not(:last-child) {
            padding: 10px 0 10px 20px !important;
            border-bottom: 1px dotted rgba(0, 0, 0, 0.2); }
        .ic-invoice .ic-invoice-body table td:first-child {
            padding-left: 0 !important; }
        .ic-invoice .ic-invoice-body table td:last-child {
            padding-top: 10px !important; }
        .ic-invoice .ic-invoice-body .form-control {
            color: #474747;
            text-align: right;
            direction: ltr; }

        .ic-invoice .ic-invoice-body .ic-textarea-description {
            outline: none;
            margin-top: 5px;
            overflow: hidden !important;
            font-size: 12px !important; }
        .ic-invoice-body .inner{
            display: flex;
        }
        .ic-invoice .ic-invoice-body .ic-invoice-sidebar {
            width: 30%;
            display: inline-block;
            padding-left: 25px;
            float: right;
            border-left: 1px dotted rgba(0, 0, 0, 0.2);
            text-align: right;
        }
        .ic-invoice .ic-invoice-body .ic-invoice-sidebar .form-group {
            max-width: 215px; }
        .ic-invoice .ic-invoice-body .ic-invoice-sidebar .ic-single-block-asidebar {
            margin-bottom: 25px; }
        .ic-invoice .ic-invoice-body .ic-invoice-sidebar .ic-invoice-label {
            font-size: 14px;
            color: #34495e !important;
            font-weight: bold; }
        .ic-invoice .ic-invoice-body .ic-invoice-sidebar .ic-invoice-totoal {
            font-size: 36px;
            line-height: 1; }

        .ic-invoice .ic-invoice-body .ic-invoice-content {
            width: 69%;
            height: 643px; }
        .ic-invoice .ic-invoice-body .ic-invoice-content input, .ic-invoice .ic-invoice-body .ic-invoice-content label {
            min-width: 80px;
            max-width: 120px; }
        .ic-invoice .ic-invoice-body .ic-invoice-content .ic-single-block-contentbar {
            margin-bottom: 25px; }
        .ic-invoice .ic-invoice-body .ic-invoice-content .ic-invoice-label {
            font-size: 14px;
            color: #34495e !important;
            font-weight: bold; }
        .ic-invoice .ic-invoice-body .ic-invoice-content .ic-
        .form-control {
            color: #474747;
            text-align: right;
            direction: ltr; }
        .ic-invoice .ic-invoice-body .ic-totoal,
        .ic-invoice .ic-invoice-body .ic-for-totoal {
            border-top: 1px dotted rgba(0, 0, 0, 0.2);
            padding: 10px 0 10px 20px; }
        .ic-invoice .ic-invoice-body .col-one-inner,
        .ic-invoice .ic-invoice-body .col-two-inner {
            padding: 10px 0 10px 20px; }

        .ic-add-discount-full {
            position: relative; }

        .tax-and-discount {
            width: 100%;
            display: table;
            margin-right: 33px;
        }
        .tax-and-discount .col-one {
            width: 63%;
            text-align: right;
            display: table-cell;
        }
        .tax-and-discount .col-two {
            width: 38%;
            text-align: right;
            margin-right: 41px;
            display: table-cell;
        }


        /*# sourceMappingURL=style.css.map */

        /** rady page css **/
        .ic-more-action-full #t-more{
            margin-left: 0 !important;
        }

        .ic-ready-page .ic-invoice .ic-left h2{
            font-weight: 400;
        }
        .ic-font-style{
            font-size: 13px;
            line-height: 1.5;
        }
        .ic-ready-page .ic-invoice .ic-invoice-body table td:last-child {
            border-bottom: 1px dotted rgba(0, 0, 0, 0.2);
            text-align: right;
        }
        table td:last-child {
            text-align: right;
        }
        table tr td:last-child {
            text-align: right;
        }
        a.ic-home{
            width: 100%;
            background: transparent;
            color: #0d83dd !important;
            font-size: 16px;
            text-align: left;
            margin-left: 0;
            margin-top: 5px;
            margin-bottom: 5px;
            opacity: .8;
            cursor: pointer;
        }
        .ic-ready-page .ic-single-block-asidebar{
            line-height: 1.3;
        }
        .ic-ready-page .ic-top-heading{
            margin-top: 20px !important;
        }
        .ic-bottom-notsterms{
            width: 100%;
            clear: both;
            padding-left: 30px;
        }
        .ic-tax-container{
            width: 100%;
            overflow: hidden;
            padding-left: 30px;
        }
    </style>
</head>
<body id="body">
<main>
    <div class="full-page ic-ready-page">
        <div class="container">
            <form>
                <div class="ic-invoice-wrapper">
                    <!-- invoice -->
                    <section class="ic-invoice p-0">
                        <div class="wrapper">
                            <div class="heading">
                                <div class="ic-form-inner">
                                    <div class="ic-left">
                                        <div class="form-group">
                                            <h2 class="mb-2">{{ $credit_memo->company_info['company_name'] }}</h2>
                                            {{ $credit_memo->company_info['phone_no'] }}
                                        </div>
                                    </div>
                                    <div class="ic-right">
                                        <div class="inner-text-ic-right ic-white ic-font-style">
                                            {{ $credit_memo->company_info['street'] }}<span> <br></span>{{ $credit_memo->company_info['city'] }} - {{ $credit_memo->company_info['zipcode'] }}<br>
                                            <span class="js-sender-address-country"><span>{{ $credit_memo->company_info['country'] }}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- invoice banner -->
                            <div class="ic-invoice-banner">
                                <div class="inner">
                                    <img src="{{ (!is_null($credit_memo->bg_image))? './storage/'.$credit_memo->bg_image : './images/support.png' }}" id="banner-img"/>
                                </div>
                            </div>
                            <!-- invoice banner end-->
                            <!-- invoice body -->
                            <div class="ic-invoice-body">
                                <div class="inner">
                                    <!-- sidebar -->
                                    <aside class="ic-invoice-sidebar">
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar">
                                            <div class="ic-text ic-invoice-label">Credit Amount (USD)</div>
                                            <div class="ic-invoice-totoal">-${{ $credit_memo->grand_total }}</div>
                                        </div>
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar ic-add-client-main">
                                            <div class="ic-text ic-invoice-label">Billed To</div>
                                            <div class="ic-billed-to-text">
                                                <span class="">{{ $credit_memo->client_info['first_name'] ?? '' . $credit_memo->client_info['last_name']  ?? '' }}</span><br>
                                                <span class="">{{ $credit_memo->client_info['company'] ?? '' }}</span><br>
                                                <span class="">{{ $credit_memo->client_info['street'] ?? '' }}</span><br>
                                                <span class="">{{ $credit_memo->client_info['city'] ?? '' }}</span><span> - </span><span class="">{{ $credit_memo->client_info['zipcode'] ?? '' }}</span><br>
                                                <span>{{ $credit_memo->client_info['country'] ?? '' }}</span>
                                            </div>
                                        </div>
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar">
                                            <div class="ic-text ic-invoice-label">Credit Number</div>
                                            <div class="ic-invoice-number">{{ $credit_memo->credit_memo_no }}</div>
                                        </div>
                                        <div class="ic-single-block-asidebar">
                                            <div class="ic-text ic-invoice-label">Invoice Number</div>
                                            <div class="ic-invoice-number">{{ $credit_memo->invoice_no }}</div>
                                        </div>
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar">
                                            <div class="ic-text ic-invoice-label">Date of Issue</div>
                                            <div class="ic-dat-of-issue">{{ $credit_memo->issue_date }}</div>
                                        </div>
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar">
                                            <div class="ic-text ic-invoice-label">Due Date</div>
                                            <div class="ic-due-date">{{ $credit_memo->due_date }}</div>
                                        </div>
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar">
                                            <div class="ic-text ic-invoice-label">Reference</div>
                                            <div class="ic-referrence">{{ $credit_memo->reference }} </div>
                                        </div>
                                    </aside>
                                    <article class="ic-invoice-content">
                                        <div class="app">
                                            <table class="table">
                                                <tr>
                                                    <th style="width:30%" class="ic-text ic-invoice-label">Description</th>
                                                    <th  style="width:25%" class="ic-text ic-invoice-label">Rate</th>
                                                    <th  style="width:20%" class="ic-text ic-invoice-label">Qty</th>
                                                    <th style="width:20%" class="ic-text ic-invoice-label">Line Total</th>
                                                </tr>
                                                @foreach($credit_memo->cart_items as $cart_item)
                                                    <tr>
                                                        <td  style="width:30%" >
                                                            <div class="ic-item-name">{{ $cart_item['name'] }}</div>
                                                            <div class="ic-input-description">{{ $cart_item['description'] }}</div>
                                                        </td>
                                                        <td style="width:25%">
                                                            <div class="ic-rate">-${{ $cart_item['rate'] }}</div>
                                                        </td>
                                                        <td style="width:20%">
                                                            <div class="ic-qty">{{ $cart_item['quantity'] }}</div>
                                                        </td>
                                                        <td style="width:20%;">
                                                            <div class="ic-total" style="text-align: center">-${{ $cart_item['rate'] * $cart_item['quantity'] }}</div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                        <div class="ic-tax-container">
                                            <div class="tax-and-discount">
                                                <div class="col-one">
                                                    <div class="col-one-inner">
                                                        <div>subtotal</div>
                                                        <div class="ic-add-discount-full">
                                                            Discount
                                                        </div>
                                                        <div class="ic-add-discount-full ic-tax">
                                                            Tax
                                                        </div>
                                                    </div>
                                                    <div class="ic-totoal">Total</div>
                                                </div>
                                                <div class="col-two">
                                                    <div class="col-two-inner">
                                                        <div class="ic-subtotal">-${{ $credit_memo->subtotal }}</div>
                                                        <div class="ic-discount-text">${{ $credit_memo->discount }}</div>
                                                        <div class="ic-tax-text" ><span>-${{ $credit_memo->tax }}</span></div>
                                                    </div>
                                                    <div class="ic-for-totoal">-${{ $credit_memo->grand_total }}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ic-bottom-notsterms">
                                            <!-- single block -->
                                            @if(!is_null($credit_memo->notes))
                                                <div class="ic-single-block-contentbar">
                                                    <div class="ic-text ic-invoice-label">Notes</div>
                                                    <div class="ic-notes">{{ $credit_memo->notes }}</div>
                                                </div>
                                            @endif
                                            <!-- single block -->
                                            @if(!is_null($credit_memo->terms))
                                                <div class="ic-single-block-contentbar">
                                                    <div class="ic-text ic-invoice-label">Terms</div>
                                                    <div class="ic-terms">{{ $credit_memo->terms }}</div>
                                                </div>
                                            @endif
                                        </div>
                                    </article>
                                </div>
                            </div>
                            <!-- invoice body end-->
                        </div>
                    </section>
                    <!-- invoice end -->
                </div>
            </form>
        </div>
    </div>
</main>
<!-- Required JavaScript Libr
aries -->

<!-- script -->
</body>
</html>
