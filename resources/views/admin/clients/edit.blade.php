@extends('admin.layout.app')

@section('page_title','Admin | Edit client')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\ClientController@create') }}">Create client</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Edit client</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">
                @include('flash::message')
                <form id="sliderGroup" action="{{ action('Admin\ClientController@update',$client->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Edit Client </h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>

                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Client Data
                                            </legend>
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="name" value="{{ $client->name }}" placeholder="Full Name" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" name="email" value="{{ $client->email }}" placeholder="Email Address" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Password (Optional)</label>
                                                <input type="password" class="form-control" name="password" placeholder="XXXXXXXXXX"  />
                                            </div>
                                            <div class="form-group">
                                                <label>Phone No</label>
                                                <input type="text" class="form-control" name="phone_no" value="{{ $client->phone_no }}" placeholder="Phone No" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Company Name</label>
                                                <input type="text" class="form-control" name="company_name" value="{{ $client->company_name }}" placeholder="Company Name" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Business Contact First Name</label>
                                                <input type="text" class="form-control" name="business_contact_first_name" value="{{ $client->business_contact_first_name }}" placeholder="Business Contact First Name" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Business Contact Last Name</label>
                                                <input type="text" class="form-control" name="business_contact_last_name" value="{{ $client->business_contact_last_name }}" placeholder="Business Contact Last Name" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Business Position</label>
                                                <input type="text" class="form-control" name="business_position" value="{{ $client->business_position }}" placeholder="Business Position" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Street</label>
                                                <input type="text" class="form-control" name="street" value="{{ $client->street }}" placeholder="Street" required />
                                            </div>
                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control" name="city" value="{{ $client->city }}" placeholder="City" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Zipcode</label>
                                                <input type="text" class="form-control" name="zipcode" value="{{ $client->zipcode }}" placeholder="Zipcode" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Country</label>
                                                <select class="form-control" name="country" required>
                                                    <option value="Bangladesh" {{ ($client->country == 'Bangladesh') ? 'selected' : '' }}>Bangladesh</option>
                                                    <option value="India" {{ ($client->country == 'India') ? 'selected' : '' }}>Bangladesh</option>
                                                    <option value="Pakisthan" {{ ($client->country == 'Pakisthan') ? 'selected' : '' }}>Pakisthan</option>
                                                    <option value="USA" {{ ($client->country == 'USA') ? 'selected' : '' }}>USA</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" name="status">
                                                    <option value="1" {{ ($client->status ==1) ?? 'selected' }}>Active</option>
                                                    <option value="0" {{ ($client->status !=1) ?? 'selected' }}>Inactive</option>
                                                </select>
                                            </div>
                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-3">
                            <!-- /well -->
                            <div class="well padding-10">
                                <h5 class="mt-0"><i class="fa fa-tags"></i> Profile Image(130 X 130):</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-body text-center">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                                    <img src="@if(!is_null($client->profile_image)){{ '/storage/' .$client->profile_image }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="profile image">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                        <input type="file" name="profile_image">
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /well -->
                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            // For Multiple Select
            if($.fn.select2) {
                $("select.select2").each(function(){var e=$(this),t=e.attr("data-select-width")||"100%";e.select2({allowClear:!0,width:t}),e=null});
            }
            if ($.fn.select2) {
                $("select.select2").each(function () {
                    var e = $(this), t = e.attr("data-select-width") || "100%";
                    e.select2({allowClear: !0, width: t}), e = null
                });
            }

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    // title : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider title is required'
                    //         },
                    //     }
                    // },
                    // subtitle : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider subtitle is required'
                    //         }
                    //     }
                    // }
                }
            });

            // end profile form

            $('div.alert').delay(3000).fadeOut(350);
        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
