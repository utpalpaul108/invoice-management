
<div class="sa-aside-left">

    <a href="javascript:void(0)"  onclick="SAtoggleClass(this, 'body', 'sa-shortcuts-expanded')" class="sa-sidebar-shortcut-toggle">
        <img src="/ic_admin/img/avatars/sunny.png" alt="" class="online">
        <span>{{ Auth::user()->name }} <span class="fa fa-angle-down"></span></span>
    </a>
    <div class="sa-left-menu-outer">
        <ul class="metismenu sa-left-menu" id="menu1">
            {{--            For Dashboard           --}}
            <li class="{{ request()->is('admin') ? 'active' : '' }}">
                <a class="has-arrow"   href="{{ action('Admin\DashboardController@index') }}" title="Dashboard"><span class="fa fa-lg fa-fw fa-home"></span> <span class="menu-item-parent">Dashboard</span></a>
            </li>

            {{--            For Clients           --}}
            <li class="{{ request()->is('admin/clients*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Clients</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/clients') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ClientController@index') }}" title="All Clients"> Clients </a>
                    </li>
                    <li class="{{ request()->is('admin/clients/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ClientController@create') }}" title="Create Client"> Create Client</a>
                    </li>
                </ul>
            </li>

            {{--            For Products           --}}
            <li class="{{ request()->is('admin/products*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Product"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Products</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/products') ? 'active' : '' }}">
                        <a href="{{ action('Admin\ProductController@index') }}" title="All Products"> Products </a>
                    </li>
                    <li class="{{ request()->is('admin/products/create*') ? 'active' : '' }}">
                        <a href="{{ action('Admin\ProductController@create') }}" title="Create Product"> Create Product</a>
                    </li>
                </ul>
            </li>

            {{--            For Invoice           --}}
            <li class="{{ request()->is('admin/invoices*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Invoice"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Invoice</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/invoices') ? 'active' : '' }}">
                        <a href="{{ action('Admin\InvoiceController@index') }}" title="All Invoices"> Invoices </a>
                    </li>
                    <li class="{{ request()->is('admin/invoices/create*') ? 'active' : '' }}">
                        <a href="{{ action('Admin\InvoiceController@create') }}" title="Create Invoice"> Create Invoice</a>
                    </li>
                </ul>
            </li>

            {{--            For Credit Memo           --}}
            <li class="{{ request()->is('admin/credit-memo*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Credit Memo"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Credit Memo</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/credit-memo') ? 'active' : '' }}">
                        <a href="{{ action('Admin\CreditMemoController@index') }}" title="All Credit Memo"> Credit Memo </a>
                    </li>
                    <li class="{{ request()->is('admin/credit-memo/create*') ? 'active' : '' }}">
                        <a href="{{ action('Admin\CreditMemoController@create') }}" title="Create Credit Memo"> Create Credit Memo </a>
                    </li>
                </ul>
            </li>

            {{--            For Payment           --}}
{{--            <li class="{{ request()->is('admin/payments*') ? 'active' : '' }}">--}}
{{--                <a class="has-arrow"   href="" title="Payments"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Payment</span>--}}
{{--                    <b class="collapse-sign">--}}
{{--                        <em class="fa fa-plus-square-o"></em>--}}
{{--                        <em class="fa fa-minus-square-o"></em>--}}
{{--                    </b>--}}
{{--                </a>--}}
{{--                <ul aria-expanded="true" class="sa-sub-nav collapse">--}}
{{--                    <li class="{{ request()->is('admin/payments') ? 'active' : '' }}">--}}
{{--                        <a href="{{ action('Admin\ClientPaymentController@index') }}" title="All Payments"> Payments </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

            {{--            For Site Settings          --}}
            <li class="{{ request()->is('admin/site-settings*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="FAQ"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Site Settings</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/site-settings') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SiteSettingController@index') }}" title="site settings"> Edit Settings </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <a href="javascript:void(0)" class="minifyme" onclick="SAtoggleClass(this, 'body', 'minified')">
        <i class="fa fa-arrow-circle-left hit"></i>
    </a>
</div>
