@component('mail::message')

{{ $mail_info['message'] }}

@component('mail::button', ['url' => $mail_info['url']])
View Credit Memo
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
