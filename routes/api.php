<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix'=>'v-1','namespace'=>'Api'],function (){
    Route::get('/company-info','UserController@company_info');
    Route::get('/products','ProductController@index');
    Route::get('/clients','UserController@clients');
    Route::post('/create-invoice','InvoiceController@store');
    Route::post('/sent-invoice','InvoiceController@sent_invoice');
    Route::post('/create-credit-memo','CreditMemoController@store');
    Route::post('/sent-credit-memo','CreditMemoController@sent_credit_memo');
    Route::post('/login','UserController@login');
    Route::group(['middleware'=>['auth:api','isActive']],function (){
//        For User Authentication
        Route::get('/logout','UserController@logout');
        Route::get('/profile','UserController@profile');

    });
});

