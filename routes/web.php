<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// For Admin Authentication
Route::get('/', 'Admin\UserController@login')->name('login');
Route::get('/admin-logout', 'Admin\UserController@logout');
Route::post('/admin-authenticate', 'Admin\UserController@authenticate');
Route::get('/invoices/{id}','Admin\InvoiceController@show');
Route::get('/download-invoice-pdf/{id}','Admin\InvoiceController@download_pdf');
Route::get('/credit-memo/{id}','Admin\CreditMemoController@show');
Route::get('/download-credit-memo-pdf/{id}','Admin\CreditMemoController@download_pdf');



// For Admin Panel
// ===============================

Route::group(['prefix'=>'/admin', 'middleware'=>'isAdmin', 'namespace'=>'Admin'],function (){

    Route::get('/', 'DashboardController@index');

//    For Clients
//    =====================================
    Route::resource('/clients','ClientController');

//    For Products
//    =====================================
    Route::resource('/products','ProductController');

//    For Invoice
//    =====================================
    Route::resource('/invoices','InvoiceController')->except('show');

//    For Credit Memo
//    =====================================
    Route::resource('/credit-memo','CreditMemoController')->except('show');

//    For Client Payments
//    =====================================
    Route::resource('/payments','ClientPaymentController');

//    For Site Settings
//    =====================================
    Route::get('/site-settings', 'SiteSettingController@index');
    Route::post('/site-settings', 'SiteSettingController@update');

});

