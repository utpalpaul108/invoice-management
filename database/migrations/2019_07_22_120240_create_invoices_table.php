<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id');
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->string('email');
            $table->string('phone_no')->nullable();
            $table->text('address')->nullable();
            $table->string('business_contact_first_name')->nullable();
            $table->string('business_contact_last_name')->nullable();
            $table->string('business_position')->nullable();
            $table->string('notes')->nullable();
            $table->string('terms')->nullable();
            $table->string('bg_image')->nullable();
            $table->string('invoice_no')->nullable();
            $table->string('issue_date')->nullable();
            $table->string('due_date')->nullable();
            $table->string('reference')->nullable();
            $table->string('subtotal')->nullable();
            $table->string('tax')->nullable();
            $table->string('discount')->nullable();
            $table->string('grand_total')->nullable();
            $table->string('paid_amount')->nullable();
            $table->string('due_amount')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
