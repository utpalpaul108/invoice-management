<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditMemosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_memos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('credit_memo_id')->nullable();
            $table->unsignedBigInteger('client_id');
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('company_info')->nullable();
            $table->text('cart_items')->nullable();
            $table->string('subtotal')->nullable();
            $table->string('discount')->nullable();
            $table->string('tax')->nullable();
            $table->string('grand_total')->nullable();
            $table->string('notes')->nullable();
            $table->string('terms')->nullable();
            $table->string('bg_image')->nullable();
            $table->string('invoice_no')->nullable();
            $table->string('credit_memo_no')->nullable();
            $table->string('issue_date')->nullable();
            $table->string('due_date')->nullable();
            $table->string('reference')->nullable();
            $table->text('client_info')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_memos');
    }
}
